<?php
if(!isset($_SESSION)) { session_start();  }

include_once __DIR__.'/../model/Files.php';
include_once __DIR__.'/../model/Mysql.php';
include_once __DIR__.'/Funciones.php';
include_once __DIR__.'/../view/Config.php';

try {
    $_SESSION['modelo'] = recoge('modelo');
    if (recoge("database") === "active") {
        $modelo = modelo();
        $modelo->instalar();
        echo "<p>Base de datos instalada con exito.</p>";
    }
    header('Location: ../view/VistaPrincipal.php');
} catch (Exception $e) {
    echo "<p>Ocurrio un problema durante la instalacion de la base de datos.</p>";
}

