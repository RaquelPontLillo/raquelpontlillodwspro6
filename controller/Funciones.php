<?php
if(!isset($_SESSION)) { session_start();  }

include_once __DIR__.'/../view/Config.php';
include_once __DIR__.'/../model/Files.php';
include_once __DIR__.'/../model/Mysql.php';

function recoge($valor) {
    $resultado = "";
    
    if (isset($_REQUEST[$valor])) {
        $resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
    }
    
    return $resultado;
}

function modelo() {
    $modelo =  $_SESSION["modelo"];
    switch ($modelo) {
        case 'ficheros':
            return new Files();
        case 'mysql':
            return new Mysql();
        default:
            return null;
    }
}
