<?php
class Config {
    public static $titulo = "MATRÍCULAS APP. 2016-2017";
    public static $autor = "Raquel Pont";
    public static $fecha = "18/01/2017";
    public static $empresa = "KandelSoft S.L.";
    public static $anio = "2016-2017";
    public static $bdnombre = "RaquelPontLillo";
    public static $bdusuario = "alumno";
    public static $bdclave = "alumno";
    public static $bdhostname = "localhost";
}
?>
