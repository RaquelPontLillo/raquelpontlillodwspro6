<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Vista de alumnos | Matrículas App. 2016-2017</title>
        <link rel="stylesheet" href="../media/css/pure-min.css">
        <link rel="shortcut icon" href="../media/images/kandel.ico">
        <meta charset="UTF-8">
        <style>
            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #333;
            }

            li {
                float: left;
            }

            li a {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            li a:hover {
                background-color: #111;
            }
        </style>
    </head>
    <body>
        <?php
        include_once __DIR__.'/Partials.php';
        include_once __DIR__.'/../controller/ControladorAlumno.php';
        mymenu();
        myheader();
        ?>
        <div class="pure-g">
            <div class="pure-u-1-12">
                <form obsubmit="<?php guardar(); ?>" method="post" class="pure-form pure-form-stacked" >
                    <table>
                        <tr>
                            <th>ID:</th>
                            <td><input type="text" name="id" value="<?php calcularID() ?>" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th>Nombre:</th>
                            <td><input type="text" name="nombre" value=""  /></td>
                        </tr>
                        <tr>
                            <th>Curso:</th>
                            <td>
                                <select name="curso" >
                                    <option value="0">-- Escoge un curso --</option>
                                    <?php cargaCombo(); ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit" class="pure-button pure-button-primary">Dar de alta</button>
                            </td>
                        </tr>
                    </table>
                </form>
                
                <table class="pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>ID curso</th>
                            <!--<th colspan="2">Acciones</th>-->
                        </tr>
                    </thead>
                    <?php listar(); ?>
                </table>     
            </div>
        </div>
<?php myfooter(); ?>
    </body>
</html>
