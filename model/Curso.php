<?php

/**
 * Description of Curso
 *
 * @author Raquel Pont <raquel.pont.lillo@gmail.com>
 */
class Curso {
    private $id;
    private $nombre;
    private $horas;

    public function __CONSTRUCT($id, $nombre, $horas) {
	$this->id = $id;
	$this->nombre = $nombre;
	$this->horas = $horas;
    }
    
    public function __GET($k) {
	return $this->$k;
    }

    public function __SET($k, $v) {
	return $this->$k = $v;
    }
}
