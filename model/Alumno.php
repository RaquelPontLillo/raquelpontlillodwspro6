<?php

/**
 * Description of Alumno
 *
 * @author Raquel Pont <raquel.pont.lillo@gmail.com>
 */
class Alumno {
    private $id;
    private $nombre;
    private $curso;

    public function __CONSTRUCT($id, $nombre, $curso) {
	$this->id = $id;
	$this->nombre = $nombre;
	$this->curso = $curso;
    }
    
    public function __GET($k) {
	return $this->$k;
    }

    public function __SET($k, $v) {
	return $this->$k = $v;
    }
}
