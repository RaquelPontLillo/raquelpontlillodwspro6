<?php
include_once __DIR__.'/Model.php';
/**
 * Description of Mysql
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
class Mysql implements Model {

    public function conectar() {
        $pdo = null;
        try {
            $pdo = new PDO("mysql:host=localhost;dbname=" . Config::$bdnombre, Config::$bdusuario, Config::$bdclave);
            $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
        } catch (PDOException $e) {
            echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
            echo "<p>Error: " . $e->getMessage() . "</p>\n";
        }
        return $pdo;
    }

    public function desconectar() {
        return null;
    }

    public function instalar() {
        try {
            $pdo = new PDO("mysql:host=localhost", Config::$bdusuario, Config::$bdclave);
            $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
        } catch (PDOException $e) {
            echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
            echo "<p>Error: " . $e->getMessage() . "</p>\n";
        }

        $consulta = "CREATE DATABASE IF NOT EXISTS " . Config::$bdnombre;
        $pdo->query($consulta);
        $pdo = $this->desconectar();

        $pdo = $this->conectar();
        $consulta = "CREATE TABLE cursos ("
                . "id INT AUTO_INCREMENT,"
                . "nombre VARCHAR(25),"
                . "horas INT(3),"
                . "PRIMARY KEY (id));"
                
                . "CREATE TABLE alumnos ("
                . "id INT AUTO_INCREMENT,"
                . "nombre VARCHAR(25),"
                . "curso INT,"
                . "PRIMARY KEY (id),"
                . "FOREIGN KEY (curso) REFERENCES cursos(id));";
        $pdo->query($consulta);

        $consulta = "INSERT INTO cursos (nombre, horas) VALUES ('Matematicas', '80');"
                . "INSERT INTO cursos (nombre, horas) VALUES ('Ingles', '40');"
                . "INSERT INTO alumnos (nombre, curso) VALUES ('Luis', 1);"
                . "INSERT INTO alumnos (nombre, curso) VALUES ('Marta', 2);";
        $pdo->query($consulta);
        $pdo = $this->desconectar();
    }
    
    public function desinstalar() {
        
    }

    public function createCurso($curso) {
        $pdo = $this->conectar();
        $consulta = "INSERT INTO cursos (nombre, horas) VALUES (:nombre, :horas);";
        $pdo->prepare($consulta)->execute(array(":nombre" => $curso->__GET('nombre'), 
            ":horas" => $curso->__GET('horas')));
        $pdo = $this->desconectar();
    }

    public function readCursos() {
        $cursos = array();
        $pdo = $this->conectar();
        $consulta = "SELECT * FROM cursos";
        $stm = $pdo->prepare($consulta);
        $stm->execute();
        foreach ($stm->fetchAll(PDO::FETCH_OBJ) as $row) {
            $curso = new Curso(0, "", "");
            $curso->__SET('id', $row->id);
            $curso->__SET('nombre', $row->nombre);
            $curso->__SET('horas', $row->horas);

            $cursos[] = $curso;
        }
        $pdo = $this->desconectar();
        return $cursos;
    }

    public function createAlumno($alumno) {
        $pdo = $this->conectar();
        $consulta = "INSERT INTO alumnos (nombre, curso) VALUES (:nombre, :curso);";
        $pdo->prepare($consulta)->execute(array(":nombre" => $alumno->__GET('nombre'),
            ":curso" => $alumno->__GET('curso')->__GET('id')
        ));
        $pdo = $this->desconectar();
    }

    public function readAlumnos() {
        $alumnos = array();
        $pdo = $this->conectar();
        $consulta = "SELECT * FROM alumnos";
        $stm = $pdo->prepare($consulta);
        $stm->execute();
        foreach ($stm->fetchAll(PDO::FETCH_OBJ) as $row) {
            $alumno = new Alumno(0, null, null);
            $curso = new Curso(0, null, null);

            $curso->__SET('id', $row->curso);
            $alumno->__SET('id', $row->id);
            $alumno->__SET('nombre', $row->nombre);
            $alumno->__SET('curso', $curso);

            array_push($alumnos, $alumno);
        }
        $pdo = $this->desconectar();
        return $alumnos;
    }

    public function idCurso() {
        $pdo = $this->conectar();
        $id = $pdo->query("SELECT MAX(id) AS id FROM cursos")->fetch(PDO::FETCH_NUM);
        $pdo = $this->desconectar();
        return $id[0] + 1;
    }

    public function idAlumno() {
        $pdo = $this->conectar();
        $id = $pdo->query("SELECT MAX(id) AS id FROM alumnos")->fetch(PDO::FETCH_NUM);
        $pdo = $this->desconectar();
        return $id[0] + 1;
    }

}
