<?php

include_once __DIR__.'/Alumno.php';
include_once __DIR__.'/Curso.php';
include_once __DIR__.'/Model.php';

/**
 * Description of Files
 *
 * @author Raquel Pont <raquel.pont.lillo@gmail.com>
 */
class Files implements Model {

    private $falumnos = "../media/database/alumnos.csv";
    private $fcursos = "../media/database/cursos.csv";

    public function instalar() {
        if (!file_exists("../media/database/cursos.csv")) {
	    $archivo = fopen("../media/database/cursos.csv", "w");
	    fclose($archivo);
            $this->createCurso(new Curso(1,"Historia",35));
            $this->createCurso(new Curso(2,"Ingles",40));
            $this->createCurso(new Curso(3,"Fisica",30));
            $this->createCurso(new Curso(4,"Lengua",50));
            $this->createCurso(new Curso(5,"Matematicas",25));
            $this->createCurso(new Curso(6,"Tecnologia",15));   
	}
        
	if (!file_exists("../media/database/alumnos.csv")) {
	    $archivo = fopen("../media/database/alumnos.csv", "w");
	    fclose($archivo);
            $this->createCurso(new Alumno(1,"Marta",1));
            $this->createCurso(new Alumno(2,"Luis",2));
            $this->createCurso(new Alumno(3,"Carmen",3));
            $this->createCurso(new Alumno(4,"Carlos",6));
            $this->createCurso(new Alumno(5,"Marcos",5));
            $this->createCurso(new Alumno(6,"Eva",4));
	}
    }
    
    public function desinstalar() {
        if (file_exists("../media/database/cursos.csv")) {
	    unlink("../media/database/cursos.csv");
	}
        
	if (file_exists("../media/database/alumnos.csv")) {
	    unlink("../media/database/alumnos.csv");
	}
    }
    
    public function createAlumno($alumno) {
        $f = fopen($this->falumnos, "a");
        $linea = $alumno->__GET('id') . ";"
                . $alumno->__GET('nombre') . ";"
                . $alumno->__GET('curso')->__GET('id') . "\r\n";
        fwrite($f, $linea);
        fclose($f);
    }

    public function readAlumnos() {
        $alumnos = array();

        if ($archivo = fopen($this->falumnos, "r")) {
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                $curso = new Curso($token[2],null,null);
                $alumno = new Alumno($token[0], $token[1], $curso);
                array_push($alumnos, $alumno);
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
        } else {
            //errores
        }

        return $alumnos;
    }

    public function createCurso($curso) {
        $fc = fopen($this->fcursos, "a");
        $linea = $curso->__GET('id') . ";"
                . $curso->__GET('nombre') . ";"
                . $curso->__GET('horas') . "\r\n";
        fwrite($fc, $linea);
        fclose($fc);
    }

    public function readCursos() {
        $cursos = array();

        if ($archivoc = fopen($this->fcursos, "r")) {
            $tokenc = fgetcsv($archivoc, 0, ";");
            while ($tokenc) {
                $curso = new Curso($tokenc[0], $tokenc[1], $tokenc[2]);
                array_push($cursos, $curso);
                $tokenc = fgetcsv($archivoc, 0, ";");
            }
            fclose($archivoc);
        } else {
            
        }

        return $cursos;
    }

    public function idCurso() {
        $cursos = $this->readCursos();
        $ultCurso = end($cursos);
        $ultID = $ultCurso->__GET('id');
        $ultID++;
        return $ultID;
    }

    public function idAlumno() {
        $alumnos = $this->readAlumnos();
        $ultAlumno = end($alumnos);
        $ultID = $ultAlumno->__GET('id');
        $ultID++;
        return $ultID;
    }

}
